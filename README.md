# ionic-bitbucket-pipelines

This image provides all tools required to automatically build and Android app out of your ionic (and possibly also cordova) project.

It is primarily meant to be run in a CI environment, such as Bitbucket CI, but can certainly also be used to run the compilation process on any machine capable of running docker without having to go through the process of setting everything up.

## Updates

16.01.2018: New Build with newest Ionic + Cordova CLI and Android build tools.

## CI Configuration

Here is a sample bitbucket.pipelines.yml file for setting up the project and compiling it:

```yaml
image: mslourens/ionic-bitbucket-pipelines:latest

pipelines:
  default:
    - step:
        script: # Modify the commands below to build your repository.
          - npm install
          - ionic config build
          - ionic platform add android
          - ionic build android
```

It is important to manage your keystores correctly. For signing debug releases, the android build tools will automatically fall back to `~/.android/debug.keystore`, which should not be password protected.

## .gitignore

This example assumes, that your `.gitignore` looks approximately like this:

```
node_modules/
platforms/
plugins/
www/lib/
```

That will avoid checking in generated files or dependencies, that will be restored using the aforementioned commands during build.

## Build the image
```bash
sudo docker build -t "someTag" .
```

## Running the build image
```bash
sudo docker run -v <localAppDir>:/app -ti --rm -p 8100:8100 -p35729:35729 someTag:latest
```
